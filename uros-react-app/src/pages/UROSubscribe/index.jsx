import Navbar from '../../components/Navbar'
import SubscriptionForm from '../../components/SubscriptionForm'
import UnsubscriptionForm from '../../components/UnsuscribeForm'
import UROSBranding from '../../components/Branding'

import '../App/App.css'

function UROSubscribe() {
  return (
    <>
      <div>
        <SubscriptionForm />
        <UnsubscriptionForm />
      </div>
      <UROSBranding />
      <Navbar />
    </>
  )
}

export default UROSubscribe
