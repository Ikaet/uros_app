import Navbar from '../../components/Navbar'

import Branding from '../../components/Branding'
import RenderData from '../../components/RenderData'
import RenderDataMongo from '../../components/RenderDataMongo'
import '../App/App.css'

function UROSMonitor() {

  return (
    <>
      <RenderData />
      <RenderDataMongo />
      <Branding />
      <Navbar />
    </>
  )
}

export default UROSMonitor
