import Navbar from '../../components/Navbar'
import ConnectionForm from '../../components/ConnectionForm'
import UROSBranding from '../../components/Branding'

import '../App/App.css'

function UROSConnection() {

  return (
    <>
      <div>
        <ConnectionForm />
      </div>
      <UROSBranding />
      <Navbar />
    </>
  )
}

export default UROSConnection
