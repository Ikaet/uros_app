import { useRoutes, BrowserRouter} from 'react-router-dom'

import Home from '../Home'
import UROSConnection from '../UROSConnection'
import UROSubscribe from '../UROSubscribe'
import UROSPublish from '../UROSPublish'
import UROSMonitor from '../UROSMonitor'
import UROSNotFound from '../UROSNotFound'
import Credits from '../Credits'

import Disclaimer from '../../components/Disclaimer'
import './App.css'

const AppRoutes = () =>{
  let routes = useRoutes([
    { path: '/', element: <Home /> },
    { path: '/UROSConnection', element: <UROSConnection /> },
    { path: '/UROSubscribe', element: <UROSubscribe /> },
    { path: '/UROSPublish', element: <UROSPublish /> },
    { path: '/UROSMonitor', element: <UROSMonitor /> },
    { path: '/Credits', element: <Credits /> },
    { path: '/*', element: <UROSNotFound /> },
  ])

  return routes
}

const App = () => {

  return(
    <BrowserRouter>
      <AppRoutes />
      <Disclaimer />
    </BrowserRouter>
  )
}

export default App
