import Navbar from '../../components/Navbar'
import PublishForm from '../../components/PublishForm'
import UnpublishForm from '../../components/UnpublishForm'
import UrosRunForm from '../../components/UrosRunForm'
import UROSBranding from '../../components/Branding'

import '../App/App.css'

function UROSPublish() {
  return (
    <>
      <div>
        <PublishForm />
        <UnpublishForm />
        <UrosRunForm />
      </div>
      <UROSBranding />
      <Navbar />
    </>
  )
}

export default UROSPublish
