import { Link } from 'react-router-dom'

import urosLogo from '../../assets/dev_uros_color_logo_v1.svg'
import '../App/App.css'

function Home() {

  return (
    <>
      <div>
        <a href="/">
          <img src={urosLogo} className="logo uros" alt="UROS logo" />
        </a>
      </div>
      <h1>UNREAL ENGINE + ROS</h1>
      <div className="card">
        <button>
          <Link to="/UROSConnection">Iniciar</Link>
        </button>
      </div>
    </>
  )
}

export default Home
