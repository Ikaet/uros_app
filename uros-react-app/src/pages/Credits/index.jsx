import Navbar from '../../components/Navbar'
import Branding from '../../components/Branding'
import CreditSource from '../../components/CreditSource'

import '../App/App.css'

function Credits() {

  return (
    <>
      <CreditSource />
      <Branding />
      <Navbar />
    </>
  )
}

export default Credits
