import Navbar from '../../components/Navbar'

import urosNotFound from '../../assets/dev_uros_notFound_v1.svg'
import UROSBranding from '../../components/Branding'
import '../App/App.css'

function UROSNotFound() {

  return (
    <>
      <div>
        <a href="/">
          <img src={urosNotFound} className="logo uros react" alt="Not Found" />
        </a>
      </div>
      <UROSBranding />
      <Navbar />
    </>
  )
}

export default UROSNotFound
