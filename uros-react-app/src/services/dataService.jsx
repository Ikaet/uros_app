// dataService.jsx

import { Mongoose } from 'mongoose';

const uri = 'mongodb://root:root123@localhost:27019/?authMechanism=DEFAULT';
const client = Mongoose;

async function connectToDatabase() {
  try {
    await client.connect(uri, {
        dbName: 'uros_api',
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, );
    console.log('Conexión exitosa a la base de datos');
    return client.db('uros_api');
  } catch (error) {
    console.error('Error al conectar a la base de datos:', error);
    throw error;
  }
}

async function obtenerDatosDeMongoDB() {
  const db = await connectToDatabase();
  const collection = db.collection('uros_unreal_arm_control/command');
  
  try {
    const result = await collection.find({}).toArray();
    return result;
  } catch (error) {
    console.error('Error al obtener datos de MongoDB:', error);
    throw error;
  } finally {
    client.close();
  }
}

export {obtenerDatosDeMongoDB};
