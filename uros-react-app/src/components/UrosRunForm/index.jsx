import { useState } from 'react';

const UrosRunForm = () => {
  const [selectedController, setSelectedController] = useState('basicUROSumoBotControl');

  const handleSelectChange = (event) => {
    setSelectedController(event.target.value);
  };

  const handleUrosRunController = async () => {
    try {
        // console.log(selectedController)
        const response = await fetch('http://localhost:8000/uros/controllers/run', {
            method: "POST",
            body: JSON.stringify({ robot_control: selectedController }),
            headers: {
                "Content-Type": "application/json",
            },
        });
        if (response.ok) {
            // If connection ok
            const data = await response.json();
            // Handle answer
            console.log(data.message);
        }else {
            console.error("Error:", response.statusText);
        }
    } catch (error) {console.error(error);}
  };

  const handleUrosStopController = async () => {
    try {
      const response = await fetch('http://localhost:8000/uros/controllers/stop', {
        method: "POST",
        body: JSON.stringify({ robot_control: selectedController }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
        console.error(error);
    }
  };

  const handleUrosControllerSaveMongo = async () => {
    try {
      const response = await fetch('http://localhost:8000/uros/controllers/save', {
        method: "POST",
        body: JSON.stringify({ robot_control: selectedController }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
        console.error(error);
    }
  };

  return (
    <div>
      <select value={selectedController} onChange={handleSelectChange}>
        <option value="basicUROSumoBotControl">basicUROSumoBotControl</option>
        <option value="basicUROSArmBotPickPlace">basicUROSArmBotPickPlace</option>
        <option value="basicUROStringMessage">basicUROStringMessage</option>
      </select>
      <div className="card">
        <button className="button bg-zinc-900 text-cyan-200 mr-2" type="button" onClick={handleUrosRunController}>
            Play Controller
        </button>
        <button className="button bg-zinc-900 text-cyan-200 mr-2" type="button" onClick={handleUrosStopController}>
            Stop
        </button>
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleUrosControllerSaveMongo}>
            Save MongoDB
        </button>
      </div>
    </div>
  );
};

export default UrosRunForm;
