import { useState } from "react";

const UnsubscriptionForm = () => {
  const [formData, setFormData] = useState({
    topic: ""
  });

  const handleUnsubscription = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/unsubscribe", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error en la solicitud de conexión:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="card form-div">
      <label htmlFor="topic">Topic Name:</label>
      <input type="text" id="topic" name="topic" required value={formData.topic} onChange={handleChange} /><br />
      
      <div className="card">
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleUnsubscription}>
          Unsubscribe
        </button>
      </div>
    </div>
  );
};

export default UnsubscriptionForm;