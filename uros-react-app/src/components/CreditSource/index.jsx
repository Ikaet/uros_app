const CreditSource = () => {
    return (
      <div className="credit-source">
        <p className="read-the-docs">
          <strong>UROS Developer - Author: </strong>
          Anibal Atahualpa Guerrero
        </p>
        <p className="read-the-docs">
          <strong>TFM: </strong>
          Desarrollo de un Laboratorio Virtual de Robots Utilizando un Motor Gráfico para Videojuegos
        </p><br />
        <p className="read-the-docs">
          <strong>Máster Universitario en Automática e Informática Indutrial</strong>
        </p>
        <p className="read-the-docs">
          <strong>Universidad Politécnica de Valencia - UPV - España - 2023</strong>
        </p><br />
        <p className="read-the-docs">
          <strong>CREDITS AND MENTIONS:</strong>
        </p>
        <br />
        <ul>
          <li className="read-the-docs">
            <ul>
              <li>
                <a href="https://roslibpy.readthedocs.io/en/latest/authors.html">ROSLIBPY Authors</a>
              </li>
              <li>Gramazio Kohler Research @gramaziokohler, Gonzalo Casas casas@arch.ethz.ch @gonzalocasas</li>
              <li>Mathias Lüdtke @ipa-mdl, Beverly Lytle @beverlylytle, Alexis Jeandeau @jeandeaual</li>
              <li>Hiroyuki Obinata @obi-t4, Pedro Pereira @MisterOwlPT</li>
              <li>Domenic Rodriguez @DomenicP</li>
            </ul>
          </li>
          <br />
          <li className="read-the-docs">
            <strong>FREEPIK RESOURCES:</strong>
            <ul>
              <li>
                Logo UROS - image from <a href="https://www.freepik.es/vector-gratis/plantilla-logotipo-hacker-creativo_21077540.htm#query=logo%20robotic&position=0&from_view=search&track=ais">Freepik</a>
              </li>
            </ul>
          </li>
          <br />
          <li className="read-the-docs">
            <strong>FONT RESOURCES:</strong>
            <ul>
              <li>
                Font:<a href="https://fonts.google.com/specimen/Barlow"> Barlow</a>
              </li>
              <li>
                Author: Jeremy Tribby
              </li>
            </ul>
          </li>
        </ul>
      </div>
    );
  };
  
  export default CreditSource;
  