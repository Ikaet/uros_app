import { useState } from "react";

const PublishForm = () => {
  const [formData, setFormData] = useState({
    topic: "",
    message_type: "",
    message_data: "",
  });

  const handlePublish = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/publish", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="card form-div">
      <label htmlFor="topic">Topic Name:</label>
      <input type="text" id="topic" name="topic" required value={formData.topic} onChange={handleChange} /><br />

      <label htmlFor="message_type">Message Type:</label>
      <input type="text" id="message_type" name="message_type" required value={formData.message_type} onChange={handleChange} /><br />
      
      <label htmlFor="message_data">Data:</label>
      <input type="text" id="message_data" name="message_data" required value={formData.message_data} onChange={handleChange} /><br />
      
      <div className="card">
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handlePublish}>
          Publish
        </button>
      </div>
    </div>
  );
};

export default PublishForm;