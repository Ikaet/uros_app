import { NavLink } from "react-router-dom"

const links = [
    { to: '/', text: 'Home' },
    { to: '/UROSConnection', text: 'ROS Connection' },
    { to: '/UROSubscribe', text: 'ROS Subscribe' },
    { to: '/UROSPublish', text: 'ROS Publish' },
    { to: '/UROSMonitor', text: 'ROS Monitor' },
    { to: '/Credits', text: 'Credits' },
  ];
  
  const Navbar = () => {
    return (
      <nav>
        <ul className="navbar-list">
            {links.map((link, index) => (
                <li key={index}>
                    <NavLink to={link.to}>
                        <div className="card">
                            <button>
                                {link.text}
                            </button>
                        </div>
                    </NavLink>
                </li>
            ))}
        </ul>
      </nav>
    );
  };

export default Navbar