import { useState } from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top',
    },
    title: {
      display: true,
      text: '/basicUROSArmBotPickPlace/joints',
    },
  },
};

const labels = ['1s', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', '11s', '12s'];
const dataFill = [
  ['0', '0', '0'],
  ['0', '0', '0'],
  ['1', '0.5', '-0.5'],
  ['1', '0.5', '-0.5'],
  ['1', '0.5', '-0.5'],
  ['0.5', '-0.2', '0.3'],
  ['0.5', '-0.2', '0.3'],
  ['0.5', '-0.2', '0.3'],
  ['0', '0', '0'],
  ['0', '0', '0'],
  ['1', '0.5', '-0.5'],
  ['1', '0.5', '-0.5'],
  ['1', '0.5', '-0.5'],
];

const RenderData = () => {
  
  const getChartData = (selectedJoint) => {
    if (selectedJoint === 'All Joints') {
      return {
        labels,
        datasets: [
          {
            label: 'Joint 1',
            data: dataFill.map((values) => parseFloat(values[0])),
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
          },
          {
            label: 'Joint 2',
            data: dataFill.map((values) => parseFloat(values[1])),
            borderColor: 'rgb(53, 162, 235)',
            backgroundColor: 'rgba(53, 162, 235, 0.5)',
          },
          {
            label: 'Joint 3',
            data: dataFill.map((values) => parseFloat(values[2])),
            borderColor: 'rgb(128, 0, 128)',
            backgroundColor: 'rgba(128, 0, 128, 0.5)',
          },
        ],
      };
    }

    const index = ['Joint 1', 'Joint 2', 'Joint 3'].indexOf(selectedJoint);
    if (index === -1) {
      return {}; // No se encontró el joint seleccionado
    }
  
    const filteredData = dataFill.map((values) => parseFloat(values[index]));
    return {
      labels,
      datasets: [
        {
          label: selectedJoint,
          data: filteredData,
          borderColor: index === 0 ? 'rgb(255, 99, 132)' : index === 1 ? 'rgb(53, 162, 235)' : 'rgb(128, 0, 128)',
          backgroundColor: `rgba(${index === 0 ? '255, 99, 132' : index === 1 ? '53, 162, 235' : '128, 0, 128'}, 0.5)`,
        },
      ],
    };
  };
  
  const [selectedValue, setSelectedValue] = useState('All Joints');
  const [chartData, setChartData] = useState(getChartData('All Joints'));

  const handleSelectChange = (event) => {
    const selectedJoint = event.target.value;
    setSelectedValue(selectedJoint);
    const newChartData = getChartData(selectedJoint);
    setChartData(newChartData);
  };
  
  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight: '400px', // Establece el tamaño máximo aquí (por ejemplo, 400px)
  };

  return (
    <div style={containerStyle}>
      <select value={selectedValue} onChange={handleSelectChange}>
        <option value="Joint 1">Joint 1</option>
        <option value="Joint 2">Joint 2</option>
        <option value="Joint 3">Joint 3</option>
        <option value="All Joints">All Joints</option>
      </select>
      <Line options={options} data={chartData} />
    </div>
  );
};

export default RenderData;
