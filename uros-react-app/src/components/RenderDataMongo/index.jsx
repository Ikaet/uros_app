import { useState, useEffect, useCallback } from "react";

const RenderDataMongo = () => {
  const [topicName, setTopicName] = useState("");
  const [limit, setLimit] = useState(5);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(() => {
    setLoading(true);
    fetch(`http://localhost:8000/uros/mongodb/data?topic_name=${topicName}&limit=${limit}`)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error al obtener los datos:", error);
        setLoading(false);
      });
  }, [topicName, limit]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const clearData = () => {
    setData([]);
    setTopicName("");
    setLimit(5);
  };

  const mongodb_data = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    maxHeight: '400px', // Establece el tamaño máximo aquí (por ejemplo, 400px)
  };

  return (
    <div className={mongodb_data}>
      <h2 className="mt-5 mb-5 font-bold">MongoDB Data for Topic:</h2>
      <div className="input-container">
        <label className="ml-2">
          Topic Name:
          <input
            className="ml-2 mr-2"
            type="text"
            value={topicName}
            onChange={(e) => setTopicName(e.target.value)}
          />
        </label>
        <label className="ml-2">
          Limit:
          <input
            className="ml-2 mr-2"
            type="number"
            value={limit}
            onChange={(e) => setLimit(e.target.value)}
          />
        </label>
        <button className="mr-2" onClick={fetchData}>Obtener Datos</button>
        <button onClick={clearData}>Limpiar Datos</button>
        <br />-------------------------------------------------------------
      </div>
      {loading ? (
        <p>Cargando datos...</p>
      ) : (
        <ul>
          {data.map((item) => (
            <li key={item._id}>
              <pre>{JSON.stringify(item, null, 2)}</pre>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default RenderDataMongo;
