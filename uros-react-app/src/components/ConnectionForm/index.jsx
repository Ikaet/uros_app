import { useState } from "react";

const ConnectionForm = () => {
  const [formData, setFormData] = useState({
    ip: "",
    port: "",
  });
  const [connected, setConnected] = useState(false); // Handle connection status

  const handleConnect = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/connect", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
        setConnected(true); // Is connected
      } else {
        console.error("Error en la solicitud de conexión:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleDisconnect = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/disconnect", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If disconnection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
        setConnected(false); // Is disconnected
      } else {
        console.error("Error en la solicitud de desconexión:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="card form-div">
      <label htmlFor="ip">IP ROS Server:</label>
      <input type="text" id="ip" name="ip" required value={formData.ip} onChange={handleChange} /><br />

      <label htmlFor="port">Port ROS Server:</label>
      <input type="number" id="port" name="port" required value={formData.port} onChange={handleChange} /><br />

      <div className="card">
        {/* <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleConnect} disabled={connected}>
          Connect to ROS
        </button> */}
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleConnect}>
          Connect
        </button>
      </div>

      <div className="card">
        {/* <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleDisconnect} disabled={!connected}>
          Disconnect
        </button> */}
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleDisconnect}>
          Disconnect
        </button>
      </div>
      <div className="">
        {connected ? (
          <p className="connected-message">Status Online</p>
        ) : (
          <p className="disconnected-message">Status Offline</p>
        )}
      </div>
    </div>
  );
};

export default ConnectionForm;