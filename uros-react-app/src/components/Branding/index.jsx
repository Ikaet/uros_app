import urosHLogo from '../../assets/dev_uros_white_logo_v2.svg'
import upvLogo from '../../assets/cp_upv_imagotipo_logo_v2.png'

import '../../pages/App/App.css'

function UROSBranding() {
    return (
    <>
        <div className='logo-container'>
            <img src={urosHLogo} className="logo-h" alt="UROS logo" />
            <img src={upvLogo} className="logo-upv" alt="UPV logo" />
        </div>
    </>
  )
}

export default UROSBranding