import { useState } from "react";

const SubscriptionForm = () => {
  const [formData, setFormData] = useState({
    topic: "",
    message_type: "",
  });

  const handleSubscription = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/subscribe", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If connection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleSubscriptionSave = async () => {
    try {
      const response = await fetch("http://localhost:8000/uros/mongodb/save", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (response.ok) {
        // If disconnection ok
        const data = await response.json();
        // Handle answer
        console.log(data.message);
      } else {
        console.error("Error:", response.statusText);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const handleChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div className="card form-div">
      <label htmlFor="topic">Topic Name:</label>
      <input type="text" id="topic" name="topic" required value={formData.topic} onChange={handleChange} /><br />

      <label htmlFor="message_type">Message Type:</label>
      <input type="text" id="message_type" name="message_type" required value={formData.message_type} onChange={handleChange} /><br />
      <div className="card">
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleSubscription}>
          Subscribe
        </button>
      </div>
      <div className="card">
        <button className="button bg-zinc-900 text-cyan-200" type="button" onClick={handleSubscriptionSave}>
          Subscribe Save MongoDB
        </button>
      </div>
    </div>
  );
};

export default SubscriptionForm;